package penadidik.csi.tabviewpager.fragment

import android.support.v4.app.Fragment
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.add_fragment.view.*
import penadidik.csi.tabviewpager.R

class Appointment: Fragment() {
    private var textFragment = ""

    companion object {
        fun newInstance(text: String): Appointment{
            val fragment = Appointment()
            val bundle = Bundle()
            bundle.putString("AddFragment", text)
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        textFragment = arguments?.get("AddFragment").toString()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.add_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        view.tvFragment.text = textFragment
    }
}