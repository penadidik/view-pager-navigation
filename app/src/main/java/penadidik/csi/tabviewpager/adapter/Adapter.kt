package penadidik.csi.tabviewpager.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter

/**
 * Created by penadidik on 11/11/2019.
 */
class Adapter(manager: FragmentManager) : FragmentPagerAdapter(manager) {

    private val fragments = ArrayList<Fragment>()
    private val titles = ArrayList<String>()

    override fun getItem(position: Int): Fragment = fragments[position]

    override fun getCount(): Int = fragments.size

    override fun getPageTitle(position: Int): CharSequence? = titles[position]

    fun Booking(fragment: Fragment, title: String) {
        fragments.add(fragment)
        titles.add(title)
    }

    fun Appointment(fragment: Fragment, title: String) {
        fragments.add(fragment)
        titles.add(title)
    }

    fun History(fragment: Fragment, title: String) {
        fragments.add(fragment)
        titles.add(title)
    }
}