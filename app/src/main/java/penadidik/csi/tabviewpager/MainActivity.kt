package penadidik.csi.tabviewpager
/**
 * Created by penadidik on 11/11/2019.
 */
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import penadidik.csi.tabviewpager.adapter.Adapter
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*
import penadidik.csi.tabviewpager.fragment.Appointment
import penadidik.csi.tabviewpager.fragment.Booking
import penadidik.csi.tabviewpager.fragment.History

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        fab.setOnClickListener { view ->
            Snackbar.make(view, "This Is Sample Program For TabLayout with ViewPager..!!", Snackbar.LENGTH_LONG).setAction("Action", null).show()
        }

        setupViewPager(pager)
        tabs.setupWithViewPager(pager)
    }

    private fun setupViewPager(pager: ViewPager?) {
        val adapter = Adapter(supportFragmentManager)

        val f1 = Booking.newInstance("Booking")
        adapter.Booking(f1, "Booking")

        val f2 = Appointment.newInstance("List Appointment")
        adapter.Appointment(f2, "List Appointment")

        val f3 = History.newInstance("History")
        adapter.History(f3, "History")

        pager?.adapter = adapter
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menuProfile ->
                showToast("You clicked Profile")

            R.id.menuPassword ->
                showToast("You clicked Password")

            R.id.menuTerm ->
                showToast("You clicked Term")

            R.id.menuFaq ->
                showToast("You clicked FAQ")
        }
        return true
    }

    private fun showToast(msg: String) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
    }
}